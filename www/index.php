<?php

echo "Hello from Docker!";

if($_GET['name']) {
    echo "<br>".PHP_EOL;
    echo "Hi, {$_GET['name']}, nice to meet you!";

    try {
        saveNameHistory($_GET['name']);
    }
    catch(Exception $e) {
        echo "--------------------<br>\n";
        echo "<b>ERROR</b>!<br>\n";
        echo $e->getMessage();
        echo "--------------------<br>\n";
    }
}


function saveNameHistory($name_)
{
    $db = new mysqli("db", "dbuser1", "dbpass1", "mytest");
    if(!$db) {
        throw new Exception("Can't connect to DB");
    }

    $stmt = $db->prepare("INSERT INTO people SET `name`=?");
    $stmt->bind_param("s", $name_);
    $stmt->execute();
}
