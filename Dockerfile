FROM php:7.4-apache

# update packages
RUN apt-get update

# add vi to edit files (only to figure out the htaccess temporarily
#RUN apt-get install -y vim

# add cron support
RUN	apt-get install -y cron

# assure the log file exists
#RUN echo "test" >> /var/log/lastlog

# Start cron service
RUN service cron start

# create a simple task
COPY crontab /var/www/html/mycrontab
RUN crontab /var/www/html/mycrontab

# enable apache's rewrite module
#RUN ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load
RUN a2enmod rewrite

# Add php extension for mysql
RUN docker-php-ext-install mysqli

# add our files
# COPY .htaccess /var/www/html/
# COPY index.php /var/www/html/

# open port 80
EXPOSE 80
